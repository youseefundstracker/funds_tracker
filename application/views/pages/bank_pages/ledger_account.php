<?php
?>
<style type="text/css">
  .mand{
        color:red;
    }
    label{
        width:145px;
    }
    .searchfrom label{
        width:105px;
    }

</style>
<div ng-app ="ledger_accountsApp" class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <div ng-controller ="Ledger_accountCtrl as ledger_accountCtrl">
        <div ng-show="!ledger_accountCtrl.addflag && !ledger_accountCtrl.updateflag" class="row">
          <input type="button" class="btn btn-primary" ng-click="ledger_accountCtrl.addflag=1" value="Add Ledger Account" />
        </div>
        <form class="form-inline" ng-show="ledger_accountCtrl.addflag || ledger_accountCtrl.updateflag" ng-submit="ledger_accountCtrl.add()" name="addLedger_account">            
            <div class="col-lg-12 col-md-12" style="padding: 0px;">
                <div class="form-group  col-lg-4 col-md-6">
                    <label ><span class="mand"></span>Account Name:</label>
                    <input type="text" class="form-control" ng-model="ledger_accountCtrl.newLedger_account.ledger_account_name" required/>              
                </div>
                
                <div class="form-group col-lg-4 col-md-6">
                    <label >Account Type</label>
                    <input type="text" class="form-control" ng-model="ledger_accountCtrl.newLedger_account.account_type" required/>              
                </div>
            </div>

            <input type="submit" class="btn btn-primary" value="{{ledger_accountCtrl.getSubmitButtonText()}}" ng-disabled="addLedger_account.$invalid">
            <input type="button" class="btn btn-primary" value="Cancel" ng-click="ledger_accountCtrl.cancelUpdate()" ng-show="ledger_accountCtrl.updateflag || ledger_accountCtrl.addflag">
        </form> 

    <div class="row">
        <!-- <br>  -->
        <!-- <h3><b> Search</b></h3>
            <div class="searchfrom ">
                <form class="form-inline" name="search_ledgeraccounts" ng-submit="ledger_accountCtrl.searchLedgerAccounts()">
                        <div class="from-group col-md-5 " >
                            <label>Account Name</label>
                            <input type="text" class="form-control" style="margin-left:40px;" ng-model="ledger_accountCtrl.searchLedger_account.ledger_account_name" ng-required="!ledger_accountCtrl.searchLedger_account.ledger_account_id"/>
                        </div>  
                        <div class="from-group col-md-5" >
                            <label >Account ID.</label>
                            <input type="text" class="form-control " style="margin-left:40px;" ng-model="ledger_accountCtrl.searchLedger_account.ledger_account_id" ng-required="ledger_accountCtrl.searchLedger_account.ledger_account_name"/>
                        </div>  
                    <input type="submit" class="btn btn-primary" value="Search"   ng-disabled="search_ledgeraccounts.$invalid"/>
                </form>
            </div>
        </div> -->
        <div style="margin-top: 10px;">
            <div class="panel panel-yellow ">
                <div class="panel-heading">Ledger Account</div>
                <table class="table table-hover tableevenodd">
                    <thead>
                        <tr>                    
                            <th>Ledger Account ID</th>
                            <th>Ledger Account Name</th>
                            <th>Account Type</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="active">
                            <td ng-bind="ledger_accountCtrl.newLedger_account.ledger_account_id"></td>  
                            <td ng-bind="ledger_accountCtrl.newLedger_account.ledger_account_name"></td>
                            <td ng-bind="ledger_accountCtrl.newLedger_account.account_type"></td>                
                        </tr>
                        <tr ng-repeat="ledger_account in ledger_accountCtrl.ledger_accounts" ng-click="ledger_accountCtrl.editRecord(ledger_account,$index)">
                            <td ng-bind="ledger_account.ledger_account_id"></td>                
                            <td ng-bind="ledger_account.ledger_account_name"></td>
                            <td ng-bind="ledger_account.account_type"></td>                
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <script  src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.11/angular.js"></script>
    <script>
        angular.module('ledger_accountsApp', [])
            .controller('Ledger_accountCtrl', ['$http', function($http) {
                $("#LefNaveLedger_account").addClass("active");
                var self = this;
                self.ledger_acounts = [];
                
        self.newLedger_account = {};
                self.updateflag = 0;
                self.addflag = 0;

                self.ledger_account_index ;
                self.ledger_account_editing ;
        self.searchLedger_account = {};


                self.cancelUpdate = function(){
                    self.newLedger_account = {};
                    if(self.updateflag == 1){
                        self.ledger_accounts.splice(self.ledger_account_index,0,self.ledger_account_editing);
                    }
                    self.updateflag = 0;
                    self.addflag = 0;
                    self.ledger_account_index = 0;
                    self.ledger_account_editing = {};
                };
                self.editRecord = function(record,vIndex){
                    self.addflag = 0;
                    var temp_ledger_account_editing = self.ledger_accounts.splice(vIndex,1);
                    if(self.updateflag == 1){
                        self.ledger_accounts.splice(self.ledger_account_index,0,self.ledger_account_editing);
                }
                self.ledger_account_index = vIndex;
                self.ledger_account_editing = temp_ledger_account_editing[0];

                jQuery.extend(self.newLedger_account, record);
                    self.updateflag = 1;
                };

                self.getSubmitButtonText = function(){
                    return self.updateflag == 0 ? "Add" : "Update"  ;
                };

        var fetchledger_accounts = function() {
                return $http.get('index.php/ledger_account/get_ledger_accounts').then(
                    function(response) {
                  self.ledger_accounts = response.data;                  
                }, function(errResponse) {
                  console.error(errResponse.data.msg);
                });
              };

    
              
              
  fetchledger_accounts();            
  self.add = function() {
                $http.post('index.php/ledger_account/add_ledger_account', self.newLedger_account)
                    .then(fetchledger_accounts)
                    .then(function(response) {
                      self.newLedger_account = {};
                        self.updateflag = 0;
                        self.addflag = 0;
                        self.ledger_account_index ;
                        self.ledger_account_editing ;
                    });
              };

            self.searchLedgerAccounts = function(){
                    $http.post('index.php/ledger_account/search_ledger_accounts', self.searchLedger_account).then(
                        function(response) {
                            self.ledger_accounts = response.data;
                        },
                        function(errResponse) {
                            console.error(errResponse.data.msg);
                        }
                    );
                };
              
        

            }])
            .factory('XHRCountsProv',[function(){
                var vActiveXhrCount = 0;
                return {
                    newCall : function(){
                        vActiveXhrCount++;
                    },
                    endCall : function(){
                        vActiveXhrCount--;
                    },
                    getActiveXhrCount : function(){
                        return vActiveXhrCount;
                    }
                };
            }])
            .factory('HttpInterceptor',['$q','XHRCountsProv',function($q,XHRCountsProv){
                return {
                    request : function(config){
                        XHRCountsProv.newCall();
                        $(".BusyLoopMain").removeClass("BusyLoopHide").addClass("BusyLoopShow");
                        return config;
                    },
                    requestError: function(rejection){
                        XHRCountsProv.endCall();
                        if(XHRCountsProv.getActiveXhrCount() == 0)
                        $(".BusyLoopMain").removeClass("BusyLoopShow").addClass("BusyLoopHide");
                        return $q.reject(rejection);
                    },
                    response:function(response){
                        XHRCountsProv.endCall();
                        if(XHRCountsProv.getActiveXhrCount() == 0)
                        $(".BusyLoopMain").removeClass("BusyLoopShow").addClass("BusyLoopHide");
                        return response;
                    },
                    responseError:function(rejection){
                        XHRCountsProv.endCall();
                        if(XHRCountsProv.getActiveXhrCount() == 0)
                        $(".BusyLoopMain").removeClass("BusyLoopShow").addClass("BusyLoopHide");
                        return $q.reject(rejection);
                    }
                };
            }])
            .config(['$httpProvider',function($httpProvider){
                $httpProvider.interceptors.push('HttpInterceptor');
            }]);
        </script>
</div>