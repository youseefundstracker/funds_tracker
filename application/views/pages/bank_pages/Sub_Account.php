<div ng-app ="subAccApp" class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
  <div ng-controller ="subAccCtrl as subAccCtrl">    
    <form class="form-inline"  ng-submit="subAccCtrl.updateflag ? subAccCtrl.update() : subAccCtrl.add()" name="addSub_Acc">            
      <div class="form-group">
          <label>Sub Account Name</label>
          <input type="text" class="form-control" ng-model="subAccCtrl.newSub_Account.ledger_sub_account_name" required/>              
      </div>
      <input type="submit" class="btn btn-default" value="Add" ng-disabled="addSub_Acc.$invalid" ng-if="subAccCtrl.addflag">
      <input type="submit" class="btn btn-default" value="Update" ng-disabled="addSub_Acc.$invalid" ng-if="subAccCtrl.updateflag">
      <input type="button" class="btn btn-default" value="Cancel" ng-click="subAccCtrl.cancelUpdate()" ng-show="subAccCtrl.updateflag">
    </form>

    <div class="panel panel-yellow " style="margin-top:10px; ">
      <div class="panel-heading">Ledger Sub Accounts</div>
      <table class="table table-hover tableevenodd">
        <thead>
          <tr>                    
              <th>Ledger Sub Account Id</th>
              <th>Ledger Sub Account Name</th>
          </tr>
        </thead>
        <tbody>
          <tr class="active">
              <td ng-bind="subAccCtrl.newSub_Account.ledger_sub_account_id"></td>  
              <td ng-bind="subAccCtrl.newSub_Account.ledger_sub_account_name"></td>
          </tr>
          <tr ng-repeat="ledger_sub_account in subAccCtrl.SubAccounts" ng-click="subAccCtrl.editRecord(ledger_sub_account,$index)">
              <td ng-bind="ledger_sub_account.ledger_sub_account_id"></td>                
              <td ng-bind="ledger_sub_account.ledger_sub_account_name"></td>
          </tr>
        </tbody>
      </table>
    </div>

  <script>
    angular.module('subAccApp', [])
      .controller('subAccCtrl', ['$http', function($http) {
          $("#LeftLedgerSubAccount").addClass("active");
          var self = this;
          self.sub_index ;
          self.sub_editing ;
          self.updateflag = 0;
          self.addflag = 1;
          self.SubAccounts = [];
          self.banks = [];             
          self.newSub_Account = {};

          var fetchSubAccounts = function() {
              return $http.get('index.php/ledger_sub_account/get_Sub_Acc').then(
              function(response) {
                    self.SubAccounts = response.data;                  
              },
              function(errResponse) {
                    console.error(errResponse.data.msg);
              });
          };  

          self.cancelUpdate = function(){
              self.newSub_Account = {};
              if(self.updateflag == 1){
                  self.SubAccounts.splice(self.sub_index,0,self.sub_editing);
              }
              self.updateflag = 0;
              self.addflag = 1;
              self.sub_index = 0;
              self.sub_editing = {};
          };               
          fetchSubAccounts();

          self.add = function() {
              $http.post('index.php/ledger_sub_account/add_ledger_sub_account', self.newSub_Account)
              .then(fetchSubAccounts)
              .then(function(response) {
                  self.newSub_Account = {};
                  self.updateflag = 0;
              });
          };             

          self.update = function() {
              $http.post('index.php/ledger_sub_account/update_ledger_sub_account', self.newSub_Account)
              .then(fetchSubAccounts)
              .then(function(response) {
                  self.newSub_Account = {};
                  self.addflag = 1;
                  self.updateflag = 0;
              });
          };

          self.editRecord = function(record,vIndex){
              self.addflag = 0;
              var temp_sub_editing = self.SubAccounts.splice(vIndex,1);
              if(self.updateflag == 1){
                  self.SubAccounts.splice(self.sub_index,0,self.sub_editing);
              }
              self.sub_index = vIndex;
              self.sub_editing = temp_sub_editing[0];
              jQuery.extend(self.newSub_Account, record);
              self.updateflag = 1;
          };          
      }])

        .factory('XHRCountsProv',[function(){
            var vActiveXhrCount = 0;
            return {
                newCall : function(){
                    vActiveXhrCount++;
                },
                endCall : function(){
                    vActiveXhrCount--;
                },
                getActiveXhrCount : function(){
                    return vActiveXhrCount;
                }
            };
        }])

        .factory('HttpInterceptor',['$q','XHRCountsProv',function($q,XHRCountsProv){
            return {
                request : function(config){
                    XHRCountsProv.newCall();
                    $(".BusyLoopMain").removeClass("BusyLoopHide").addClass("BusyLoopShow");
                    return config;
                },
                requestError: function(rejection){
                    XHRCountsProv.endCall();
                    if(XHRCountsProv.getActiveXhrCount() == 0)
                    $(".BusyLoopMain").removeClass("BusyLoopShow").addClass("BusyLoopHide");
                    return $q.reject(rejection);
                },  
                response:function(response){
                    XHRCountsProv.endCall();
                    if(XHRCountsProv.getActiveXhrCount() == 0)
                    $(".BusyLoopMain").removeClass("BusyLoopShow").addClass("BusyLoopHide");
                    return response;
                },
                responseError:function(rejection){
                    XHRCountsProv.endCall();
                    if(XHRCountsProv.getActiveXhrCount() == 0)
                    $(".BusyLoopMain").removeClass("BusyLoopShow").addClass("BusyLoopHide");
                    return $q.reject(rejection);
                }
            };
      }])

      .config(['$httpProvider',function($httpProvider){
          $httpProvider.interceptors.push('HttpInterceptor');
      }]);;
    </script>
  </div>
</div>

