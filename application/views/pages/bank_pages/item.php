<?php
?>
<style type="text/css">
  
  label{
    width:100px;
  }
  .mand{
    color:red;
  }

</style>
<div ng-app ="itemsApp" class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	<div ng-controller ="ItemsCtrl as itemCtrl">
		<div ng-show="!itemCtrl.addflag && !itemCtrl.updateflag" class="row">
		  <input type="button" class="btn btn-primary" ng-click="itemCtrl.addflag=1" value="Add Item" />
		</div>
		<form class="form-inline" ng-show="itemCtrl.addflag || itemCtrl.updateflag" ng-submit="itemCtrl.add()" name="addItem">            
			<div class="col-lg-12 col-md-12" style="padding: 0px;">
				<div class="form-group  col-lg-4 col-md-6">
					<label ><span class="mand"></span>Item Name</label>
					<input type="text" class="form-control" ng-model="itemCtrl.newItems.item_name" required/>              
				</div>
				<div class="form-group col-lg-6 col-md-6">
					<label >Quantity </label>
            		<input type="text" class="form-control" ng-model="itemCtrl.newItems.quantity" required />
				</div>
				<div class="form-group col-lg-4 col-md-6">
					<label >Description </label>
          			<input type="text" class="form-control"  size="20" ng-model="itemCtrl.newItems.description"/>                            
				</div>
				<div class="form-group col-lg-4 col-md-6">
        			<label >Model  </label>
          			<input type="text" class="form-control"  size="20" ng-model="itemCtrl.newItems.model" required/>              
				</div>
				<div class="form-group col-lg-4 col-md-6">
					<label >Supply Date </label>
					<input type="date" name="date" class="form-control" size="20" ng-model="itemCtrl.newItems.supply_date" required />
      			</div>            
				<div class="form-group col-lg-4 col-md-6">
					<label>Cost  </label>
					<input type="text" class="form-control" size="20" ng-model="itemCtrl.newItems.cost"required />              
				</div>
			</div>
			<input type="submit" class="btn btn-primary" value="{{itemCtrl.getSubmitButtonText()}}" ng-disabled="addItem.$invalid" >
			<input type="button" class="btn btn-primary" value="Cancel" ng-click="itemCtrl.cancelUpdate()" ng-show="itemCtrl.updateflag || itemCtrl.addflag">
		</form>
		<div style="margin-top: 10px;">
			<div class="panel panel-yellow ">
				<div class="panel-heading">Items</div>
				<table class="table table-hover tableevenodd">
					<thead>
						<tr>
							<th>Item ID</th>
							<th>Item Name</th>
							<th>Quantity</th>
							<th>Description</th>
							<th>Model</th>
							<th>Supply Date</th>
							<th>Cost</th>
						</tr>
					</thead>
					<tbody>
						<tr class="active">
							<td ng-bind="itemCtrl.newItems.item_id"></td>
							<td ng-bind="itemCtrl.newItems.item_name"></td>
							<td ng-bind="itemCtrl.newItems.quantity"></td>
							<td ng-bind="itemCtrl.newItems.description"></td>
							<td ng-bind="itemCtrl.newItems.model"></td>
							<td ng-bind="itemCtrl.newItems.supply_date | date:'dd-MMM-yyyy'"></td>
							<td ng-bind="itemCtrl.newItems.cost"></td>
						</tr>
						<tr ng-repeat="item in itemCtrl.items" ng-click="itemCtrl.editRecord(item,$index)">
							<td ng-bind="item.item_id"></td>
							<td ng-bind="item.item_name"></td>
							<td ng-bind="item.quantity"></td>
							<td ng-bind="item.description"></td>
							<td ng-bind="item.model"></td>
							<td ng-bind="item.supply_date | date:'dd-MMM-yyyy'"></td>
							<td ng-bind="item.cost"></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<script  src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.11/angular.js"></script>
	<script>
		angular.module('itemsApp', [])
			.controller('ItemsCtrl', ['$http', "formatDataFilter","DateFormater", function($http,formatData,DateFormater) {
				$("#LefNaveItem").addClass("active");
				var self = this;
				self.items = [];
				self.newItems = {};
				self.updateflag = 0;
				self.addflag = 0;

				self.item_index ;
				self.item_editing ;

				self.cancelUpdate = function(){
					self.newItems = {};
					if(self.updateflag == 1){
						self.items.splice(self.item_index,0,self.item_editing);
					}
					self.updateflag = 0;
					self.addflag = 0;
					self.item_index = 0;
					self.item_editing = {};
				};
				self.editRecord = function(record,vIndex){
					self.addflag = 0;
					var temp_item_editing = self.items.splice(vIndex,1);
					if(self.updateflag == 1){
						self.items.splice(self.item_index,0,self.item_editing);
				}
				self.item_index = vIndex;
				self.item_editing = temp_item_editing[0];

				jQuery.extend(self.newItems, record);
					self.updateflag = 1;
				};

				self.getSubmitButtonText = function(){
					return self.updateflag == 0 ? "Add" : "Update"  ;
				};

				var fetch_items = function() {
					return $http.get('index.php/items/getItemsList').then(
						function(response) {
							self.items = response.data;
						}, function(errResponse) {
							console.error(errResponse.data.msg);
						}
					);
				};
			  
				fetch_items();             

			self.add = function() {
					var tempDetails = {};
					jQuery.extend(tempDetails,self.newItems);
					tempDetails.supply_date = DateFormater.convertJsDate(tempDetails.supply_date); 
					$http.post('index.php/items/add_item', tempDetails)
					.then(fetch_items)
					.then(function(response) {
						self.newItems = {};
						self.updateflag = 0;
						self.addflag = 0;
						self.item_index = 0;
						self.item_editing = {};
					});
				};
            self.MapDate = function(vDate){
                if(vDate)
                	return vDate.getDate()+"-"+ (vDate.getMonth()+1) +"-"+vDate.getFullYear();
              };
			}])
			.filter('formatData',["DateFormater",function(DateFormater){
              return function(vItems){
                var vFormatedItem = [];
                if(vItems){
                  $.each(vItems,function(index,tempItem){
                    tempItem.vRowTempID = index ;
                    tempItem.supply_date = DateFormater.convertDtToJSDate(tempItem.supply_date) ;
                    vFormatedItem[index] = tempItem;
                  });
                }       
              return vFormatedItem;
              }
            }])
			.factory('DateFormater',[function(){
                return {
                  convertDtToJSDate : function(vStringDate){
                    if(vStringDate && vStringDate != "0000-00-00 00:00:00" && vStringDate.split('-').length > 2){
                      var vTDate = Date.parse(vStringDate);
                      return new Date(vTDate);
                      var vSplitDate = vStringDate.split(' ');
                      var vDatePart = vSplitDate[0].split('-');
                      vTDate.setFullYear(vDatePart[0]);
                      vTDate.setMonth( ( parseInt(vDatePart[1])-1) );
                      vTDate.setDate(vDatePart[2]);

                      return  vTDate;
                    }
                   else{
                      return "";
                    }
                  },
                  convertJsDate : function(vDate){
                    if(vDate) return  vDate.getFullYear()+"-"+ ("0" + (vDate.getMonth()+1)).slice(-2) +"-"+
					 ("0" + vDate.getDate()).slice(-2);
                    else return "";
                  }
                };
            }])
            .factory('XHRCountsProv',[function(){
				var vActiveXhrCount = 0;
				return {
					newCall : function(){
						vActiveXhrCount++;
					},
					endCall : function(){
						vActiveXhrCount--;
					},
					getActiveXhrCount : function(){
						return vActiveXhrCount;
					}
				};
			}])
			.factory('HttpInterceptor',['$q','XHRCountsProv',function($q,XHRCountsProv){
				return {
					request : function(config){
						XHRCountsProv.newCall();
						$(".BusyLoopMain").removeClass("BusyLoopHide").addClass("BusyLoopShow");
						return config;
					},
					requestError: function(rejection){
						XHRCountsProv.endCall();
						if(XHRCountsProv.getActiveXhrCount() == 0)
						$(".BusyLoopMain").removeClass("BusyLoopShow").addClass("BusyLoopHide");
						return $q.reject(rejection);
					},
					response:function(response){
						XHRCountsProv.endCall();
						if(XHRCountsProv.getActiveXhrCount() == 0)
						$(".BusyLoopMain").removeClass("BusyLoopShow").addClass("BusyLoopHide");
						return response;
					},
					responseError:function(rejection){
						XHRCountsProv.endCall();
						if(XHRCountsProv.getActiveXhrCount() == 0)
						$(".BusyLoopMain").removeClass("BusyLoopShow").addClass("BusyLoopHide");
						return $q.reject(rejection);
					}
				};
			}])
			.config(['$httpProvider',function($httpProvider){
				$httpProvider.interceptors.push('HttpInterceptor');
			}]);
		</script>
</div>
