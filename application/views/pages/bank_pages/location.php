
<div ng-app ="locationApp" class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
  <div ng-controller ="locationCtrl as locationCtrl">    
    <form class="form-inline"  ng-submit="locationCtrl.updateflag ? locationCtrl.update() : locationCtrl.add()" name="addLocation">            
      <div class="form-group">
          <label>Location Name</label>
          <input type="text" class="form-control" ng-model="locationCtrl.newLocation.location_name" required/>              
      </div>
      <input type="submit" class="btn btn-primary" value="Add" ng-disabled="addLocation.$invalid" ng-if="locationCtrl.addflag">
      <input type="submit" class="btn btn-primary" value="Update" ng-disabled="addLocation.$invalid" ng-if="locationCtrl.updateflag">
      <input type="button" class="btn btn-primary" value="Cancel" ng-click="locationCtrl.cancelUpdate()" ng-show="locationCtrl.updateflag">
    </form>

    <div class="panel panel-yellow " style="margin-top:10px; ">
      <div class="panel-heading">Locations</div>
      <table class="table table-hover tableevenodd">
        <thead>
          <tr>     
              <th>Location ID</th>               
              <th>Location Name</th>
          </tr>
        </thead>
        <tbody>
          <tr class="active">
                <td ng-bind="locationCtrl.newLocation.location_id"></td>
              <td ng-bind="locationCtrl.newLocation.location_name"></td>
          </tr>
          <tr ng-repeat="location in locationCtrl.locations" ng-click="locationCtrl.editRecord(location,$index)">             
              <td ng-bind="location.location_id"></td>
              <td ng-bind="location.location_name"></td>
          </tr>
        </tbody>
      </table>
    </div>

  <script>
    angular.module('locationApp', [])
      .controller('locationCtrl', ['$http', function($http) {
          $("#LefNaveLocation").addClass("active");
          var self = this;
          self.location_index ;
          self.location_editing ;
          self.updateflag = 0;
          self.addflag = 1;
          self.locations = [];            
          self.newLocation = {};

          var fetchlocations = function() {
              return $http.get('index.php/location/get_location').then(
              function(response) {
                    self.locations = response.data;                  
              },
              function(errResponse) {
                    console.error(errResponse.data.msg);
              });
          };  

          self.cancelUpdate = function(){
              self.newLocation = {};
              if(self.updateflag == 1){
                  self.locations.splice(self.location_index,0,self.location_editing);
              }
              self.updateflag = 0;
              self.addflag = 1;
              self.location_index = 0;
              self.location_editing = {};
          };               
          fetchlocations();

          self.add = function() {
              $http.post('index.php/location/add_location', self.newLocation)
              .then(fetchlocations)
              .then(function(response) {
                  self.newLocation = {};
                  self.updateflag = 0;
              });
          };             

          self.update = function() {
              $http.post('index.php/location/update_location', self.newLocation)
              .then(fetchlocations)
              .then(function(response) {
                  self.newLocation = {};
                  self.addflag = 1;
                  self.updateflag = 0;
              });
          };

          self.editRecord = function(record,vIndex){
              self.addflag = 0;
              var temp_location_editing = self.locations.splice(vIndex,1);
              if(self.updateflag == 1){
                  self.locations.splice(self.location_index,0,self.location_editing);
              }
              self.location_index = vIndex;
              self.location_editing = temp_location_editing[0];
              jQuery.extend(self.newLocation, record);
              self.updateflag = 1;
          };          
      }])

        .factory('XHRCountsProv',[function(){
            var vActiveXhrCount = 0;
            return {
                newCall : function(){
                    vActiveXhrCount++;
                },
                endCall : function(){
                    vActiveXhrCount--;
                },
                getActiveXhrCount : function(){
                    return vActiveXhrCount;
                }
            };
        }])

        .factory('HttpInterceptor',['$q','XHRCountsProv',function($q,XHRCountsProv){
            return {
                request : function(config){
                    XHRCountsProv.newCall();
                    $(".BusyLoopMain").removeClass("BusyLoopHide").addClass("BusyLoopShow");
                    return config;
                },
                requestError: function(rejection){
                    XHRCountsProv.endCall();
                    if(XHRCountsProv.getActiveXhrCount() == 0)
                    $(".BusyLoopMain").removeClass("BusyLoopShow").addClass("BusyLoopHide");
                    return $q.reject(rejection);
                },  
                response:function(response){
                    XHRCountsProv.endCall();
                    if(XHRCountsProv.getActiveXhrCount() == 0)
                    $(".BusyLoopMain").removeClass("BusyLoopShow").addClass("BusyLoopHide");
                    return response;
                },
                responseError:function(rejection){
                    XHRCountsProv.endCall();
                    if(XHRCountsProv.getActiveXhrCount() == 0)
                    $(".BusyLoopMain").removeClass("BusyLoopShow").addClass("BusyLoopHide");
                    return $q.reject(rejection);
                }
            };
      }])

      .config(['$httpProvider',function($httpProvider){
          $httpProvider.interceptors.push('HttpInterceptor');
      }]);;
    </script>
  </div>
</div>

