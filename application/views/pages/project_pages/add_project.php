<?php 
?>


<div ng-app ="projectsApp" class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <div ng-controller ="ProjectCtrl as projectCtrl">
            <div ng-show="!projectCtrl.addflag && !projectCtrl.updateflag" class="row">
                <input type="button" class="btn btn-primary" ng-click="projectCtrl.addflag=1" value="Add Project" />
            </div>
        <form class="form-inline"  ng-show="projectCtrl.addflag || projectCtrl.updateflag" ng-submit="projectCtrl.add()" name="addProject">            
            <div class="col-lg-12 col-md-12" style="padding: 0px;">
                <div class="form-group col-lg-4 col-md-6">
                    <label >Project Name</label>
                    <input type="text" class="form-control" ng-model="projectCtrl.newProject.project_name" required/>              
                </div>
            <!--<div class="form-group">
              <label >Select Project Group</label>
              <select class="form-control" ng-model="projectCtrl.newProject.project_group_id" 
                  ng-options="group.project_group_id as group.project_group_name for group in projectCtrl.project_groups">                          
              </select>
            </div> -->
            </div>
                <input type="submit" class="btn btn-primary" value="{{projectCtrl.getSubmitButtonText()}}" ng-disabled="addProject.$invalid">
                <input type="button" class="btn btn-primary" value="Cancel" ng-click="projectCtrl.cancelUpdate()" ng-show="projectCtrl.updateflag || projectCtrl.addflag">
        </form>    
       
       <div style="margin-top: 10px;">
            <div class="panel panel-yellow ">
                <div class="panel-heading">Projects</div>
                    <table class="table table-hover tableevenodd">
                    <thead>
                    <tr>              
                            <th>Project Name</th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr class="active">
                            <td ng-bind="projectCtrl.newProject.project_name"></td>	
                        </tr>
                        <tr  ng-repeat="project in projectCtrl.projects" ng-click="projectCtrl.editRecord(project,$index)">
                            <td ng-bind="project.project_name"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.11/angular.js"></script>
        <script>
          angular.module('projectsApp', [])
            .controller('ProjectCtrl', ['$http', function($http) {
		  $("#LefNaveProject").addClass("active");
              var self = this;
              self.projects = [];
              self.project_groups = [];
              self.newProject = {};
                self.updateflag = 0;
                self.addflag = 0;

                self.project_index ;
                self.project_editing ;
                self.cancelUpdate = function(){
                    self.newProject = {};
                    if(self.updateflag == 1){
                        self.projects.splice(self.project_index,0,self.project_editing);
                    }
                    self.updateflag = 0;
                    self.addflag = 0;
                    self.project_index = 0;
                    self.project_editing = {};
                };
                self.editRecord = function(record,vIndex){
                    self.addflag = 0;
                    var temp_project_editing = self.projects.splice(vIndex,1);
                    if(self.updateflag == 1){
                        self.projects.splice(self.project_index,0,self.project_editing);
                }
                self.project_index = vIndex;
                self.project_editing = temp_project_editing[0];

                jQuery.extend(self.newProject, record);
                    self.updateflag = 1;
                };

                self.getSubmitButtonText = function(){
                    return self.updateflag == 0 ? "Add" : "Update"  ;
                };
              var fetchprojects = function() {
                return $http.get('index.php/project/get_projects').then(
                    function(response) {
	                  self.projects = response.data;                  
                }, function(errResponse) {
                  console.error(errResponse.data.msg);
                });
              };
              var fetchgroups = function(){
                return $http.get('index.php/project/get_project_groups').then(
                    function(response){
                        self.project_groups = response.data;
                    },function(errResponse){
                        console.error(errResponse.data.msg);
                    }
              )};
              fetchgroups();
              fetchprojects();
              
              self.add = function() {
                $http.post('index.php/project/add_project', self.newProject)
                    .then(fetchprojects)
                    .then(function(response) {
                        self.newProject = {};
                        self.updateflag = 0;
                        self.addflag = 0;
                        self.project_index = 0;
                        self.project_editing = {};
                    });
              };
              
            }]);
            
        </script>
    </div>    
