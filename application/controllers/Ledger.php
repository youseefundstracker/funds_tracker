<?php

class Ledger extends CI_Controller {
	
    function __construct(){
        parent::__construct();

        $this->load->model('generic_model');        
        $this->load->model('ledger_model');        
    }
    
    function index(){
        //if($this->session->logged_in != 'YES'){
            redirect(base_url()+"/");   
        //}        
        //$this->load->view('nav_bars/header');
        //$this->load->view('nav_bars/left_nav');
        //$this->load->view('pages/party_pages/party');
        //$this->load->view('nav_bars/footer');
    }
	
    function getLedgerAccountList(){
//        if($this->session->logged_in != 'YES'){
//            $ResultData["Status"] = 1001;
//            $ResultData["ErroMsg"] = "Please login to access this data";
//			$this->output
//			->set_content_type('application/json')
//			->set_output(json_encode($ResultData));
//        }
//        else
			{
			$items = $this->generic_model->get_ledger_accounts_list();
			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($items));
		}
    }
	function getLedgerSubAccountList(){
//		if($this->session->logged_in != 'YES'){
//            $ResultData["Status"] = 1001;
//            $ResultData["ErroMsg"] = "Please login to access this data";
//			$this->output
//			->set_content_type('application/json')
//			->set_output(json_encode($ResultData));
//        }        
//        else
			{
			$items = $this->generic_model->get_ledger_sub_accounts_list();
			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($items));
		}
	}
	function addLedgerTransaction(){
        $ledger_transactions = $this->ledger_model->addLedgerTransaction();
		$this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($ledger_transactions));    
    }
    
    
	function getLedgerTransactions($trnxID,$type){
//		$ResultData = array();
		$ledger_transactions = $this->ledger_model->get_ledger_transactions($trnxID,$type);
		$this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($ledger_transactions));
//		}
	}
	function deleteLedgerTransaction($ledger_id){
		$ResultData = array();
		if($this->session->logged_in != 'YES'){
            $ResultData["Status"] = 1001;
            $ResultData["ErroMsg"] = "Please login to access this data";
			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($ResultData));
        }        
        else{
			$ledger_transactions = $this->ledger_model->delete_ledger_transaction($ledger_id);
			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($ledger_transactions));
		}
	}
}
?>
