<?php

class Items extends CI_Controller {    
    function __construct(){
        parent::__construct();  
        $this->load->model('item_model');        
    }
    
    function index(){
        if($this->session->logged_in != 'YES')
        {
			redirect(base_url()+"/");
        }
        $this->load->view('nav_bars/header');
        $this->load->view('nav_bars/left_nav');
        $this->load->view('pages/bank_pages/item');
        $this->load->view('nav_bars/footer');
    }

    function add_item(){      
        if($this->session->logged_in != 'YES'){
            $ResultData["Status"] = 1001;
            $ResultData["ErroMsg"] = "Please login to access this data";
			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($ResultData));
        }  
		else{	
            $item_id = $this->item_model->add_item();
            $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($item_id));
        }
    }

    function getItemsList(){
        if($this->session->logged_in != 'YES'){
            $ResultData["Status"] = 1001;
            $ResultData["ErroMsg"] = "Please login to access this data";
			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($ResultData));
        }
        else{        
            $item_information = $this->item_model->getItemsList();
            $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($item_information));
        }
    }
}

?>
