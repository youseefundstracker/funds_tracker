<?php

class Ledger_account extends CI_Controller {
    function __construct() {
        parent::__construct();        
        $this->load->model('ledger_account_model');
    }
    
    function index(){
        if($this->session->logged_in != 'YES'){
            redirect(base_url()+"/");
        }
        $this->load->view('nav_bars/header');
        $this->load->view('nav_bars/left_nav');
        $this->load->view('pages/bank_pages/ledger_account');
        $this->load->view('nav_bars/footer');
    }
    
    function add_ledger_account(){
        if($this->session->logged_in != 'YES'){
            $ResultData["Status"] = 1001;
            $ResultData["ErroMsg"] = "Please login to access this data";
            $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($ResultData));
        }        
        else{
            $ledger_account_id = $this->ledger_account_model->add_ledger_account();
            $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($ledger_account_id));
        }
    }
    function search_ledger_accounts(){
        if($this->session->logged_in != 'YES'){
            $ResultData["Status"] = 1001;
            $ResultData["ErroMsg"] = "Please login to access this data";
            $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($ResultData));
        } else{
            $ledger_account_information = $this->ledger_account_model->search_ledger_accounts();
            $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($ledger_account_information));
        }
    }
    function get_ledger_accounts(){
        if($this->session->logged_in != 'YES'){
            $ResultData["Status"] = 1001;
            $ResultData["ErroMsg"] = "Please login to access this data";
            $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($ResultData));
        } else{
            $ledger_account_information = $this->ledger_account_model->get_ledger_accounts();
            $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode( $ledger_account_information));
        }
    }
}

?>
