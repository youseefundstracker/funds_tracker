<?php

class ledger_sub_account extends CI_Controller{    
    function __construct() {
        parent::__construct();   
        if($this->session->logged_in != 'YES'){      
            redirect(base_url()+"/");
         }
         $this->load->model('SubAccount_model');
         $this->load->model('bank_account_model');
    }
    function index(){
        $this->load->view('nav_bars/header');
        $this->load->view('nav_bars/left_nav');
        $this->load->view('pages/bank_pages/Sub_Account');
        $this->load->view('nav_bars/footer');
    }
    
    function add_ledger_sub_account(){
        if($this->session->logged_in != 'YES'){
            $ResultData["Status"] = 1001;
            $ResultData["ErroMsg"] = "Please login to access this data";
            $this->output
			->set_content_type('application/json')
			->set_output(json_encode($ResultData));
        }        
        else{
            $ledger_sub_account_id = $this->SubAccount_model->add_ledger_sub_account();
            $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($ledger_sub_account_id));
        }
    }

    function update_ledger_sub_account(){
        if($this->session->logged_in != 'YES'){
            $ResultData["Status"] = 1001;
            $ResultData["ErroMsg"] = "Please login to access this data";
            $this->output
			->set_content_type('application/json')
			->set_output(json_encode($ResultData));
        }        
        else{
            $ledger_sub_account_id = $this->SubAccount_model->update_ledger_sub_account();
            $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($ledger_sub_account_id));
        }
    }

    function get_Sub_Acc(){
        if($this->session->logged_in != 'YES'){
            $ResultData["Status"] = 1001;
            $ResultData["ErroMsg"] = "Please login to access this data";
            $this->output
			->set_content_type('application/json')
			->set_output(json_encode($ResultData));
        }        
        else{
            $Sub_account_information = $this->SubAccount_model->get_Sub_Acc();
            $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($Sub_account_information));
        }    
    }
}
?>