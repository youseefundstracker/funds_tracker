<?php

class Project extends CI_Controller{
    function __construct() {
        parent::__construct();    
        $this->load->model('project_model');       
    }
    
    function index(){
        if($this->session->logged_in != 'YES'){
            redirect(base_url()+"/");
        }
        $this->load->view('nav_bars/header');
        $this->load->view('nav_bars/left_nav');
        $this->load->view('pages/project_pages/add_project');
        $this->load->view('nav_bars/footer');
    }
    
    function add_project(){  
        if($this->session->logged_in != 'YES'){
            $ResultData["Status"] = 1001;
            $ResultData["ErroMsg"] = "Please login to access this data";
			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($ResultData));
        }        
        else{      
            $project_id = $this->project_model->add_project();
            $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($project_id));
        }
    }
    
    function get_projects(){ 
        if($this->session->logged_in != 'YES'){
            $ResultData["Status"] = 1001;
            $ResultData["ErroMsg"] = "Please login to access this data";
			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($ResultData));
        }        
        else{       
            $projects_information = $this->project_model->get_projects();
            $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($projects_information));
        }
    }
    
    function get_project_groups(){
        if($this->session->logged_in != 'YES'){
            $ResultData["Status"] = 1001;
            $ResultData["ErroMsg"] = "Please login to access this data";
			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($ResultData));
        }        
        else{
            $project_groups = $this->project_model->get_project_groups();
            $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($project_groups));
        }
    }
}

?>
