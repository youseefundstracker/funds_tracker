<?php

class Location extends CI_Controller{    
    function __construct() {
        parent::__construct();   
         $this->load->model('location_model');
    }
    function index(){
        if($this->session->logged_in != 'YES'){      
            redirect(base_url()+"/");
         }
        $this->load->view('nav_bars/header');
        $this->load->view('nav_bars/left_nav');
        $this->load->view('pages/bank_pages/location');
        $this->load->view('nav_bars/footer');
    }
    
    function add_location(){
        if($this->session->logged_in != 'YES'){
            $ResultData["Status"] = 1001;
            $ResultData["ErroMsg"] = "Please login to access this data";
            $this->output
			->set_content_type('application/json')
			->set_output(json_encode($ResultData));
        }        
        else{
            $location_id = $this->location_model->add_location();
            $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($location_id));
        }
    }

    function update_location(){
        if($this->session->logged_in != 'YES'){
            $ResultData["Status"] = 1001;
            $ResultData["ErroMsg"] = "Please login to access this data";
            $this->output
			->set_content_type('application/json')
			->set_output(json_encode($ResultData));
        }        
        else{
            $location_id = $this->location_model->update_location();
            $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($location_id));
        }
    }

    function get_location(){
       
            $location_information = $this->location_model->get_location();
            $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($location_information));
            
    }
}
?>
