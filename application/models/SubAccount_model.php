<?php

class SubAccount_model extends CI_Model {
    
    private $return_size = 300;
    private $Sub_account_information = array();
    
    function __construct() {
        parent::__construct();
        $post = (array)json_decode($this->security->xss_clean($this->input->raw_input_stream));  
        if(key_exists('ledger_sub_account_id', $post)){
            $this->Sub_account_information['ledger_sub_account_id'] = $post['ledger_sub_account_id'];
        }
        if(key_exists('ledger_sub_account_name', $post)){
            $this->Sub_account_information['ledger_sub_account_name'] = $post['ledger_sub_account_name'];
        }
        $this->Sub_account_information['org_id'] = $this->session->org_id;
    }
    
    function add_ledger_sub_account(){                   
        $this->db->insert('ledger_sub_account', $this->Sub_account_information);
        return $this->db->insert_id();        
    }

    function update_ledger_sub_account(){
         $this->db->where('ledger_sub_account_id',$this->Sub_account_information['ledger_sub_account_id']);
         unset($this->Sub_account_information['ledger_sub_account_id']);
         $this->db->update('ledger_sub_account',$this->Sub_account_information);
         return true;  
    }    
    
    function get_Sub_Acc(){
        $org_id=$this->session->org_id;
        $this->db->select('*')
                ->from('ledger_sub_account')
                ->order_by('ledger_sub_account_id','DESC')
                ->where('ledger_sub_account.org_id',$org_id)
                ->limit("$this->return_size");
        $query = $this->db->get();       
        $result = $query->result();
        return $result;
    }
}
