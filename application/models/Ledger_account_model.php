<?php

class Ledger_account_model extends CI_Model {
    
    private $return_size = 300;
    private $ledger_account_information = array();
    
    function __construct() {
        parent::__construct();
        $org_id=$this->session->org_id; 
        $post = (array)json_decode($this->security->xss_clean($this->input->raw_input_stream));  
        if(key_exists('ledger_account_id', $post)){
            $this->ledger_account_information['ledger_account_id'] = $post['ledger_account_id'];
        }
        if(key_exists('ledger_account_name', $post)){
            $this->ledger_account_information['ledger_account_name'] = $post['ledger_account_name'];
        }
        if(key_exists('account_type', $post)){
            $this->ledger_account_information['account_type'] = $post['account_type'];
        }
        $this->ledger_account_information['org_id'] = $org_id;

    }
    
    function  add_ledger_account()
    { 
        $org_id=$this->session->org_id; 
        if(key_exists('ledger_account_id', $this->ledger_account_information))
        {
            $this->db->where('ledger_account_id',$this->ledger_account_information['ledger_account_id']);
            unset($this->ledger_account_information['ledger_account_id']);
            $this->db->update('ledger_account',$this->ledger_account_information);
            return true;
        }
        else
        {       
        $this->db->insert('ledger_account', $this->ledger_account_information);        
        return $this->db->insert_id();
        }
    }
    
    function get_ledger_accounts(){
        $org_id=$this->session->org_id; 
        $this->db->select('*')
                ->from('ledger_account') 
                ->where('ledger_account.org_id',$org_id)
                ->order_by('ledger_account_id','DESC')       
                ->limit("$this->return_size");
        $query = $this->db->get();

        $result = $query->result();
        return $result;
    }
    function search_ledger_accounts(){
        $org_id=$this->session->org_id; 
        $ledger_account_name = key_exists('ledger_account_name', $this->ledger_account_information) ? $this->ledger_account_information['ledger_account_name'] : "";
        $ledger_account_id = key_exists('ledger_account_id', $this->ledger_account_information) ? $this->ledger_account_information['ledger_account_id'] : "";

        $this->db->select('*')
                ->from('ledger_account')
                ->where('ledger_account.org_id',$org_id)
                ->where('ledger_account_id', $ledger_account_id);
        $query = $this->db->get();            
        $result = $query->result();            
        
        return $result;

    }

}
