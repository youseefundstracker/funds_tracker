<?php

class Project_model extends CI_Model {
    
    private $return_size = 300;
    private $project_information = array();
    
    function __construct() {
        parent::__construct();
        $org_id=$this->session->org_id;
        $post = (array)json_decode($this->security->xss_clean($this->input->raw_input_stream));
        if(key_exists('project_id',$post)){
            $this->project_information['project_id'] = $post['project_id'];
        }
        if(key_exists('project_name',$post)){
            $this->project_information['project_name'] = $post['project_name'];
        }
        /*if(key_exists('project_group_id',$post)){
            $project_information['project_group_id'] = $post['project_group_id'];
        }*/
        /*if($this->input->post('note')){
            $this->project_information['note'] = $this->input->post('note');
        }*/
        
        $this->project_information['user_id'] =$this->session->user_id;
        $this->project_information['org_id'] = $org_id;
    
    }
    
    function add_project(){  
        if(key_exists('project_id', $this->project_information))
        {
            $this->db->where('project_id',$this->project_information['project_id']);
            unset($this->project_information['project_id']);
            $this->db->update('project',$this->project_information);
            return true;
        }
        else
        {         
        $this->db->insert('project', $this->project_information);        
        return $this->db->insert_id();   
        }
    }
    
    
    function get_projects(){ 
        $org_id=$this->session->org_id;       
        $this->db->select('*')
                ->from('project')
                ->order_by('project_id','DESC') 
                ->where('project.org_id', $org_id)
                //->join('project_group','project_group.project_group_id = project.project_group_id','left')
                ->limit("$this->return_size");
        $query = $this->db->get();
        
        $result = $query->result();
        return $result;        
    }
    
    function get_project_groups(){
        $org_id=$this->session->org_id;
        $this->db->select('*')
                ->from('project_group')
                ->order_by('project_group_id','DESC') 
                ->where('project_group.org_id', $org_id)
               
        //        ->where($project_condition)
                ->limit("$this->return_size");
        $query = $this->db->get();
        
        $result = $query->result();
        return $result;  
    }
    
}
