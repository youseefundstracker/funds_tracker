<?php

class Item_model extends CI_Model {
    
    private $return_size = 300;
    private $item_information = array();    
    
    function __construct() {
        parent::__construct();
        $org_id=$this->session->org_id;
        $post = (array)json_decode($this->security->xss_clean($this->input->raw_input_stream));
        if(key_exists('item_name', $post)){
            $this->item_information['item_name'] = $post['item_name'];        
        }
        if(key_exists('quantity', $post)){
            $this->item_information['quantity'] = $post['quantity'];        
        }
        if(key_exists('description', $post)){
            $this->item_information['description'] = $post['description'];        
        }
        if(key_exists('model', $post)){
            $this->item_information['model'] = $post['model'];        
        }
        if(key_exists('supply_date', $post)){
            $this->item_information['supply_date'] = $post['supply_date'];        
        }
        if(key_exists('cost', $post)){
            $this->item_information['cost'] = $post['cost'];        
        }
        if(key_exists('item_id', $post)){
            $this->item_information['item_id'] = $post['item_id'];        
        }
        
        $this->item_information['org_id'] = $org_id;
        
    }
    
    function add_item(){
        $org_id=$this->session->org_id;
        if(key_exists('item_id', $this->item_information)){
            $this->db->where('item_id',$this->item_information['item_id']);
            unset($this->item_information['item_id']);
            $this->db->update('item',$this->item_information);
            return true;
        }else{
            $this->db->insert('item', $this->item_information);
            return $this->db->insert_id();
        }
        
    }   
    
    function getItemsList(){
        $org_id=$this->session->org_id;
        $this->db->select('*')
            ->from('item')
            ->order_by('item_id','DESC')
            ->where('item.org_id',$org_id)
            ->limit("$this->return_size");
        $query = $this->db->get();
        
        $result = $query->result();
        return $result;        
    }
    
}
