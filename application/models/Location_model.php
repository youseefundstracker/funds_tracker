
<?php

class Location_model extends CI_Model {
    
    private $return_size = 300;
    private $location_information = array();
    
    function __construct() {
        parent::__construct();
        $post = (array)json_decode($this->security->xss_clean($this->input->raw_input_stream));  
        if(key_exists('location_id', $post)){
            $this->location_information['location_id'] = $post['location_id'];
        }
        if(key_exists('location_name', $post)){
            $this->location_information['location_name'] = $post['location_name'];
        }
        $this->location_information['org_id'] = $this->session->org_id;
    }
    
    function add_location(){                   
        $this->db->insert('location', $this->location_information);
        return $this->db->insert_id();        
    }

    function update_location(){
         $this->db->where('location_id',$this->location_information['location_id']);
         $this->db->where('org_id',$this->location_information['org_id']);
         unset($this->location_information['location_id']);
         $this->db->update('location',$this->location_information);
         return true;  
    }    
    
    function get_location(){
        $org_id=$this->session->org_id;
        $this->db->select('*')
                ->from('location')
                ->order_by('location_id','DESC')
                ->where('location.org_id',$org_id)
                ->limit("$this->return_size");
        $query = $this->db->get();       
        $result = $query->result();
        return $result;
    }
}
